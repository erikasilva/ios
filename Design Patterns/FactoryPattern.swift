enum CarType{
    case bus, taxi, truck
}

protocol Car{
    func drive()
}

class Bus: Car{
    func drive(){
        print("Drive a bus!!")
    }
}

class Taxi: Car{
    func drive(){
        print("Drive a taxi!!")
    }
}

class Truck: Car{
    func drive(){
        print("Drive a truck!!")
    }
}

class CarFactory {
    static func produceCar(carType: CarType) -> Car{
        switch carType {
        case .bus:
            return Bus()
        case .taxi:
            return Taxi()
        case .truck:
            return Truck()
        }
    }
}

let bus = CarFactory.produceCar(carType: .bus)
bus.drive()
let taxi = CarFactory.produceCar(carType: .taxi)
taxi.drive()
let truck = CarFactory.produceCar(carType: .truck)
truck.drive()

