protocol Transporting {
    func getSpeed() -> Double
    func getTraction() -> Double
}

// Core Component
final class RaceCar: Transporting{
    private let speed = 10.0
    private let traction = 10.0

    func getSpeed() -> Double{
        return speed
    }

    func getTraction() -> Double{
        return traction
    }   
}

//Abstract Decorator
class TireDecorator: Transporting{
    private let transportable: Transporting

    init(transportable: Transporting){
        self.transportable = transportable
    }
    
    func getSpeed() -> Double {
        return transportable.getSpeed()
    }

    func getTraction() -> Double {
        return transportable.getTraction()
    }
}

class OffRoadTireDecorator: Transporting {
    private let transportable: Transporting

    init(transportable: Transporting) {
        self.transportable = transportable
    }

    func getSpeed() -> Double{
        return transportable.getSpeed() - 3.0
    }

    func getTraction() -> Double{
        return transportable.getTraction() + 3.0
    }
}

//Decorating Decorators
class ChainedTireDecorator: Transporting {
  private let transportable: Transporting
  
  init(transportable: Transporting) {
    self.transportable = transportable
  }
  
  func getSpeed() -> Double {
    return transportable.getSpeed() - 1.0
  }
  
  func getTraction() ->
 Double {
    return transportable.getTraction() * 1.1
  }
}


// Create Race Car
let defaultRaceCar = RaceCar()
print("Speed of Default Race Car: \(defaultRaceCar.getSpeed())")
print("Traction of Default Race Car: \(defaultRaceCar.getTraction())") 

// Modify Race Car
let offRoadRaceCar = OffRoadTireDecorator(transportable: defaultRaceCar)
print("Speed of Off Road Tire Decorator: \(offRoadRaceCar.getSpeed())")
print("Traction of Off Road Tire Decorator: \(offRoadRaceCar.getTraction())") 

// Double Decorators
let chainedTiresRaceCar  = ChainedTireDecorator(transportable: offRoadRaceCar)
print("Speed of Chained Tire Decorator: \(chainedTiresRaceCar.getSpeed())") 
print("Traction of Chained Tire Decorator: \(chainedTiresRaceCar.getTraction())")


