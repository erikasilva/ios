class WebApplication{
    fileprivate var theme: Theme
    
    init(_ theme: Theme) {
        self.theme = theme
    }
    
    func applyTheme()->String{
        return ""
    }
}

class Blog: WebApplication{
    override func applyTheme() -> String{
        return "Blog created. " + theme.applyTheme()
    }
}

class NewsSite: WebApplication{
    override func applyTheme() -> String{
        return "NewsSite created. " + theme.applyTheme()
    }
}

protocol Theme {
    func applyTheme() -> String
}

class DarkTheme: Theme{
    func applyTheme() -> String {
        return "Theme is Dark!"
    }
}

class LightTheme: Theme{
    func applyTheme() -> String {
        return "Theme is Light!"
    }
}
let lightTheme = LightTheme()
let darkTheme = DarkTheme()
let lightBlog = Blog(lightTheme)
print(lightBlog.applyTheme())
let darkBlog = Blog(darkTheme)
print(darkBlog.applyTheme())
let lightNewsSite = NewsSite(lightTheme)
print(lightNewsSite.applyTheme())
let darkNewsSite = NewsSite(darkTheme)
print(darkNewsSite.applyTheme())