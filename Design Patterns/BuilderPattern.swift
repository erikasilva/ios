{\rtf1\ansi\ansicpg1252\cocoartf1671\cocoasubrtf200
{\fonttbl\f0\fnil\fcharset0 Menlo-Bold;\f1\fnil\fcharset0 Menlo-Regular;}
{\colortbl;\red255\green255\blue255;\red252\green95\blue163;\red31\green31\blue36;\red255\green255\blue255;
\red145\green212\blue98;\red174\green243\blue125;\red252\green106\blue93;\red122\green200\blue182;\red153\green232\blue213;
}
{\*\expandedcolortbl;;\csgenericrgb\c98839\c37355\c63833;\csgenericrgb\c12054\c12284\c14131;\csgenericrgb\c100000\c100000\c100000;
\csgenericrgb\c56799\c83212\c38450;\csgenericrgb\c68215\c95290\c48909;\csgenericrgb\c98912\c41558\c36568;\csgenericrgb\c47716\c78607\c71403;\csgenericrgb\c59926\c90967\c83488;
}
\paperw11900\paperh16840\margl1440\margr1440\vieww10800\viewh8400\viewkind0
\deftab593
\pard\tx593\pardeftab593\pardirnatural\partightenfactor0

\f0\b\fs24 \cf2 \cb3 protocol
\f1\b0 \cf4  MealBuilder\{\
    
\f0\b \cf2 func
\f1\b0 \cf4  buildFrenchFries()\
    
\f0\b \cf2 func
\f1\b0 \cf4  buildDrink()\
    
\f0\b \cf2 func
\f1\b0 \cf4  buildHamburger()\
    
\f0\b \cf2 func
\f1\b0 \cf4  buildCheese()\
\}\
\

\f0\b \cf2 class
\f1\b0 \cf4  HamburgerBuilder: \cf5 MealBuilder\cf4 \{\
    
\f0\b \cf2 private
\f1\b0 \cf4  
\f0\b \cf2 var
\f1\b0 \cf4  hamburger = \cf5 Hamburger\cf4 ()\
    \
    
\f0\b \cf2 func
\f1\b0 \cf4  reset()\{\
        \cf5 hamburger\cf4  = \cf5 Hamburger\cf4 ()\
    \}\
    \
    
\f0\b \cf2 func
\f1\b0 \cf4  buildFrenchFries() \{\
        \cf5 hamburger\cf4 .\cf6 add\cf4 (part: \cf7 "French Fries"\cf4 )\
    \}\
    \
    
\f0\b \cf2 func
\f1\b0 \cf4  buildDrink() \{\
        \cf5 hamburger\cf4 .\cf6 add\cf4 (part: \cf7 "Coke Drink"\cf4 )\
    \}\
\
    
\f0\b \cf2 func
\f1\b0 \cf4  buildHamburger() \{\
        \cf5 hamburger\cf4 .\cf6 add\cf4 (part: \cf7 "Hamburger"\cf4 )\
    \}\
    
\f0\b \cf2 func
\f1\b0 \cf4  buildCheese() \{\
        \cf5 hamburger\cf4 .\cf6 add\cf4 (part: \cf7 "Cheddar Cheese"\cf4 )\
    \}\
    \
    
\f0\b \cf2 func
\f1\b0 \cf4  retrieveHamburger() -> \cf5 Hamburger\cf4 \{\
        
\f0\b \cf2 let
\f1\b0 \cf4  result = 
\f0\b \cf2 self
\f1\b0 \cf4 .\cf5 hamburger\cf4 \
        \cf6 reset\cf4 ()\
        
\f0\b \cf2 return
\f1\b0 \cf4  result\
    \}\
\}\
\

\f0\b \cf2 class
\f1\b0 \cf4  HamburgerDirector\{\
    
\f0\b \cf2 private
\f1\b0 \cf4  
\f0\b \cf2 var
\f1\b0 \cf4  hamburgerBuilder: \cf5 MealBuilder\cf4 ?\
    \
    
\f0\b \cf2 func
\f1\b0 \cf4  update(mealBuilder: \cf5 MealBuilder\cf4 )\{\
        
\f0\b \cf2 self
\f1\b0 \cf4 .\cf5 hamburgerBuilder\cf4  = mealBuilder\
    \}\
    \
    
\f0\b \cf2 func
\f1\b0 \cf4  buildSimpleHamburger()\{\
        \cf5 hamburgerBuilder\cf4 ?.\cf6 buildHamburger\cf4 ()\
        \cf5 hamburgerBuilder\cf4 ?.\cf6 buildCheese\cf4 ()\
    \}\
    \
    
\f0\b \cf2 func
\f1\b0 \cf4  buildFullHamburger()\{\
        \cf5 hamburgerBuilder\cf4 ?.\cf6 buildFrenchFries\cf4 ()\
        \cf5 hamburgerBuilder\cf4 ?.\cf6 buildDrink\cf4 ()\
        \cf5 hamburgerBuilder\cf4 ?.\cf6 buildHamburger\cf4 ()\
        \cf5 hamburgerBuilder\cf4 ?.\cf6 buildCheese\cf4 ()\
    \}\
\}\
\

\f0\b \cf2 class
\f1\b0 \cf4  Hamburger\{\
    
\f0\b \cf2 private
\f1\b0 \cf4  
\f0\b \cf2 var
\f1\b0 \cf4  parts = [\cf8 String\cf4 ]()\
    
\f0\b \cf2 func
\f1\b0 \cf4  add(part: \cf8 String\cf4 )\{\
        
\f0\b \cf2 self
\f1\b0 \cf4 .\cf5 parts\cf4 .\cf9 append\cf4 (part)\
    \}\
    \
    
\f0\b \cf2 func
\f1\b0 \cf4  listParts() -> \cf8 String\cf4 \{\
        
\f0\b \cf2 return
\f1\b0 \cf4  \cf7 "Hamburger parts: "\cf4  + \cf5 parts\cf4 .\cf9 joined\cf4 (separator: \cf7 ", "\cf4 ) + \cf7 "\\n"\cf4 \
    \}\
\}\
\

\f0\b \cf2 let
\f1\b0 \cf4  hamburgerDirector = \cf5 HamburgerDirector\cf4 ()\

\f0\b \cf2 let
\f1\b0 \cf4  hamburgerBuilder = \cf5 HamburgerBuilder\cf4 ()\
\cf5 hamburgerDirector\cf4 .\cf6 update\cf4 (mealBuilder: \cf5 hamburgerBuilder\cf4 )\
\
\cf9 print\cf4 (\cf7 "Simple hamburger: "\cf4 )\
\cf5 hamburgerDirector\cf4 .\cf6 buildSimpleHamburger\cf4 ()\
\cf9 print\cf4 (\cf5 hamburgerBuilder\cf4 .\cf6 retrieveHamburger\cf4 ().\cf6 listParts\cf4 ())\
\
\cf9 print\cf4 (\cf7 "Full Hamburger: "\cf4 )\
\cf5 hamburgerDirector\cf4 .\cf6 buildFullHamburger\cf4 ()\
\cf9 print\cf4 (\cf5 hamburgerBuilder\cf4 .\cf6 retrieveHamburger\cf4 ().\cf6 listParts\cf4 ())\
}