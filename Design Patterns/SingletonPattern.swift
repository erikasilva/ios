class AccountManager{
    static let sharedInstance = AccountManager()
    var userInfo = (id: "erika", password: 123456)
    var isAuthenticated = false

    func authenticateUser(){
        if isAuthenticated {
            print("Authenticated user")
            return
        }else{
            print("Unauthenticated user")
            isAuthenticated = true
        }

       
    }

    private init() {
        print("I'm created")
    }
}
print(AccountManager.sharedInstance.userInfo)
print(AccountManager.sharedInstance.userInfo.id)
print(AccountManager.sharedInstance.userInfo.password)
AccountManager.sharedInstance.authenticateUser()
